# Full Stack Development with Express
##	Overview
This repository contains implementation example on common features used on Full stack development with express framework.
The project is preconfigured to use `babel` for ES6 transpilation for NodeJS.

To use nodemon:
```powershell
npm run dev
```

## Prequisites
This example assumes you have familiarity with Javascript ES6, TypeScript and some concepts of SQL.

### Contents
#### External
The examples can be found under the route `/external`. These shows simple implementation of consuming external API services such as NETs QR to make payment.

#### Tabling
The examples can be found under the route `example`.
These examples contains standard use cases of tables with server side paging with Bootstrap Table.
These examples also demonstrates how to use sequelize and raw mySQL to achieve the same contents.

## Supporting documentations
1. Sequelize ORM
