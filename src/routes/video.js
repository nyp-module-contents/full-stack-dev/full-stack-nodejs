import { Router, Request, Response, NextFunction } from 'express'
import { flash_message, FlashType  } from '../helpers/flash-messenger'
import { ModelVideo, ModelUser } from '../models/models'
import { UserRole } from '../../build/models/users';
import { FileService, remove_file, Url as ImageUrl } from '../config/multer-image'
import { Sequelize } from 'sequelize/types';

/**
 * Configure router parameters
 * @see "http://expressjs.com/en/5x/api.html#express.router"
 */
const router = Router({
	caseSensitive: false,   //	Ensure that /home vs /HOME does exactly the same thing
	mergeParams  : false,   //	Cascade all parameters down to children routes.
	strict       : false    //	Whether we should strictly differenciate "/home/" and "/home"
});

/**
 * Sub routes
 * /video/create
 * /video/list
 * /video/update
 * /video/delete
**/
router.use("/",                authorizer);	//..Applies to every route in this router

router.get('/',                page_default);
router.get('/list',            page_video_list);
router.get('/create',          page_video_create);
router.get('/update/:uuid',    ensure_owner_or_admin, page_video_update);

router.put  ('/create',        FileService.single('imagePoster'), handle_video_create);
router.patch('/update/:uuid',  ensure_owner_or_admin, FileService.single('imagePoster'), handle_video_update);

//	Query = ?h=x&p=tt
//	Body  = Form Body
//	Params = http://......../yourpath/:param1/:param2/:param3 ... 
// router.get("/delete/:uuid", handle_video_delete);	//..Does this even make sense???
router.delete("/delete/:uuid", ensure_owner_or_admin, handle_video_delete);
module.exports = router;

/**
 * Authorize user
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 * @param {NextFunction} next Express next handle function
**/
function authorizer(req, res, next) {

	if (req.user === undefined || req.isUnauthenticated()) {
		return res.render("error", {
			"code"   : 401,
			"message": "Unauthorized. Please login!"
		});	//	Unauthorized
	}
	else {
		next();	// Okay No problem, allow to proceed
	}
}

/**
 * Authorizes owners
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 * @param {NextFunction} next Express next handle function
 */
 async function ensure_owner(req, res, next) {

	if (req.user === undefined || req.isUnauthenticated()) {
		return res.render("error", {
			"code"   : 401,
			"message": "Unauthorized. Please login!"
		});	//	Unauthorized
	}
	else {
		try {
			const qty = await ModelVideo.count({
				where : {
					"uuid"     : req.params["uuid"],
					"uuid_user": req.user.uuid
				}
			});
			if (qty == 1) {
				next();
			}
			else {
				return res.render("error", {
					"code"   : 403,
					"message": "Forbidden! Only the owner is allowed"
				});
			}
		}
		catch (error) {
			console.error("Failed to verify owner of content");
			console.error(error);
			return res.status(500).end();
		}
	}
}

/**
 * A authorizer owner or administrator
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 * @param {NextFunction} next Express next handle function
 */
 async function ensure_owner_or_admin(req, res, next) {

	if (req.user === undefined || req.isUnauthenticated()) {
		return res.render("error", {
			"code"   : 401,
			"message": "Unauthorized. Please login!"
		});	//	Unauthorized
	}
	else {
		try {
			const video = await ModelVideo.findByPk(req.params["uuid"]);
			if (video && (req.user.uuid == video.uuid_user || req.user.role == UserRole.Admin)) {
				next();
			}
			else {
				return res.render("error", {
					"code"   : 403,
					"message": "Forbidden! Only the owner is allowed"
				});
			}
		}
		catch (error) {
			console.error("Failed to verify owner of content");
			console.error(error);
			return res.status(500).end();
		}
	}
}

/**
 * Default page handler
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 */
function page_default(req, res) {
	return res.redirect('/video/list');
}

/**
 * Renders video list page
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 */
async function page_video_list(req, res) {
	try {
		//	TODO: Conditions to apply for WHERE
		//	You will need to adjust the count according to your required conditions if search is supported
		//	As the total number of rows will change and in turn affect the paging
		const total = await ModelVideo.count();

		const pageIdx   = req.query.page    ? parseInt(req.query.page,  10) : 1;
		const pageSize  = req.query.pageSize? parseInt(req.query.pageSize, 10) : 10;
		const pageTotal = Math.floor(total / pageSize);

		const videos = await ModelVideo.findAll({
			offset: (pageIdx - 1) * pageSize,
			limit : pageSize,
			order : [
				['title', 'ASC']
			],
			raw: true
		});		
		// videos[0].update()	//	This will crash... if raw is enabled
		return res.render('video/listVideos', {
			"videos"   : videos,
			"pageTotal": pageTotal,
			"pageIdx"  : pageIdx,
			"pageSize" : pageSize
		});
	}
	catch (error) {
		console.error("Failed to retrieve list of videos");
		console.error(error);
		return res.status(500).end();
	}
}

/**
 * Renders the video creation page
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 */
function page_video_create(req, res) {
	return res.render('video/detailVideo', {
		"pageJS" : ["/js/video-detail.js"],
		"mode"   : "create",
		"content": {}
	});
}

/**
 * Handles the creation of video
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 */
async function handle_video_create(req, res) {

	try {
		/** @type {string} */
		const csv_language = Array.isArray(req.body["language"])? req.body["language"].join(',') : req.body["language"];
		/** @type {string} */
		const csv_subtitle = Array.isArray(req.body["subtitle"])? req.body["subtitle"].join(',') : req.body["subtitle"];

		if (req.file === undefined && req.body["posterURL"] === undefined) {
			return res.status(400).end();
		}

		const new_video = await ModelVideo.create({
			"title"         : req.body["title"],
			"story"         : req.body["story"],
			"dateReleased"  : req.body["dateReleased"],
			"language"      : csv_language,
			"subtitle"      : csv_subtitle,
			"classification": req.body["classification"],
			"uuid_user"     : req.user.uuid,
			"urlImage"      : (req.file)? `${ImageUrl}/${req.file.filename}`: req.body["posterURL"]
		});

		return res.redirect("/video/list");
	}
	catch (error) {
		console.error("Failed to create new video");
		console.error(error);
		//	Clean up and remove file if error
		if (req.file) {
			console.error("Removing uploaded file");
			remove_file(req.file.filename);
		}
		flash_message(res, FlashType.Error, "Failed to create new video");
		return res.render('video/detailVideo', {
			"pageJS" : ["/js/video-detail.js"],
			"mode"   : "create",
			"content": req.body
		});
	}
}


/**
 * Renders the video update page, Basically the same page as detailVideo with
 * prefills and cancellation.
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 */
 async function page_video_update(req, res) {

	try {
		const content = await ModelVideo.findOne({where: { "uuid": req.params["uuid"] }});
		if (content) {
			return res.render('video/detailVideo', {
				"pageJS" : ["/js/video-detail.js"],
				"mode"   : "update",
				"content": content
			});
		}
		else {
			console.error(`Failed to retrieve video ${req.params["uuid"]}`);
			console.error(error);
			return res.status(410).end();
		}
	}
	catch (error) {
		console.error(`Failed to retrieve video ${req.params["uuid"]}`);
		console.error(error);
		return res.status(500).end();	//	Internal server error	# Usually should not even happen !!
	}
}

/**
 * Handles the creation of video.
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 */
async function handle_video_update(req, res) {

	try {
		//	Please verify your contents
		if (!req.body["title"])
			throw Error("Missing title");
	}
	catch(error) {
		console.error(`Malformed request to update video ${req.params["uuid"]}`);
		console.error(req.body);
		console.error(error);
		return res.status(400).end();
	}

	try {
		const contents = await ModelVideo.findAll({where: { "uuid": req.params["uuid"] } });

		//	Whether this update request need to swap files?
		const replaceFile = (req.file)? true : false;

		switch (contents.length) {
			case 0      : return res.redirect(410, "/video/list")
			case 1      : break;
			     default: return res.status(409, "/video/list")
		}
		/** @type {string} */
		req.body["language"] = Array.isArray(req.body["language"])? req.body["language"].join(',') : req.body["language"];
		/** @type {string} */
		req.body["subtitle"] = Array.isArray(req.body["subtitle"])? req.body["subtitle"].join(',') : req.body["subtitle"];
		
		//	Save previous file path...
		const previous_file = contents[0].urlImage;
		const data          = {
			"title"         : req.body["title"],
			"story"         : req.body["story"],
			"dateReleased"  : req.body["dateReleased"],
			"language"      : req.body["language"],
			"subtitle"      : req.body["subtitle"],
			"classification": req.body["classification"],
		};

		//	Assign new file if necessary
		if (replaceFile) {
			data["urlImage"] = `${ImageUrl}/${req.file.filename}`;
		}
		else if (req.body["posterURL"]) {
			data["urlImage"] = req.body["posterURL"];
		}
		
		await (await contents[0].update(data)).save();

		//	Remove old file when succeess and replacing file
		if (replaceFile) {
			remove_file(previous_file);
		}
		
		flash_message(res, FlashType.Success, "Video updated");
		return res.redirect(`/video/update/${req.params["uuid"]}`);
	}
	catch(error) {
		console.error(`Failed to update video ${req.params["uuid"]}`);
		console.error(error);

		//	Clean up and remove file if error
		if (req.file) {
			console.error("Removing uploaded file");
			remove_file(`./uploads/${req.file.filename}`);
		}
		
		flash_message(res, FlashType.Error, "The server met an unexpected error");
		return res.redirect(500, "/video/list")
	}	
}


/**
 * Handles the deletion of video.
 * @param {Request}  req Express request  object
 * @param {Response} res Express response object
 */
 async function handle_video_delete(req, res) {

	const regex_uuidv4 = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i

	if (!regex_uuidv4.test(req.params["uuid"]))
		return res.status(400);
	
	//	Perform additional checks such as whether it belongs to the current user
	/** @type {ModelUser} */
	const user = req.user;

	try {
		const targets = await ModelVideo.findAll({where: { "uuid_user": user.uuid, "uuid": req.params["uuid"] }});

		switch(targets.length) {
			case 0      : return res.status(409);
			case 1      : console.log("Found 1 eligible video to be deleted"); break;
			     default: return res.status(409);
		}
		const affected = await ModelVideo.destroy({where: { "uuid": req.params["uuid"]}});

		if (affected == 1) {
			//	Delete all files associated
			targets.forEach((target) => { 
				remove_file(target.urlImage);
			});

			console.log(`Deleted video: ${req.params["uuid"]}`);
			return res.redirect("/video/list");
		}
		//	There should only be one, so this else should never occur anyway
		else {
			console.error(`More than one entries affected by: ${req.params["uuid"]}, Total: ${affected}`);
			return res.status(409);
		}
	}
	catch (error) {
		console.error(`Failed to delete video: ${req.params["uuid"]}`);
		console.error(error);
		return res.status(500);
	}
}