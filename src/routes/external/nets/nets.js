import { Router, Request, Response, NextFunction } from 'express'
import { format_date } from '../../../helpers/format-date'
import { sha256 } from 'hash.js'
import FileSys from 'fs'
import Axios   from 'axios'

const api_nets_key  = "<Your own api key>";
const api_nets_skey = "<Your own api key>";
var   api_nets_stan = 0;

const router = Router();

router.post('/generate', handle_transaction_generate);
router.post('/query',    handle_transaction_query);
router.post('/void',     handle_transaction_void);

module.exports = router;

/**
 * Create a QR code order payload to be sent to NETs services
 * @param {number} amount The amount to charge in cents
 * @return {import('./nets-payload').OrderRequest}
 */
function create_order(amount) {
	/** @type {import('./nets-payload').OrderRequest} */
	const payload  = JSON.parse(FileSys.readFileSync(`${__dirname}/nets-qr-request.json`));
	const datetime = new Date();
	const stan     = ++api_nets_stan;

	if (api_nets_stan >= 1000000)
		api_nets_stan = 0;

	payload.amount           = `${amount}`;
	payload.npx_data.E201    = `${amount}`;
	payload.stan             = stan.toString();
	payload.transaction_date = format_date(datetime, "MMDD");
	payload.transaction_time = format_date(datetime, "HHmmss");

	return payload;
}

/**
 * Generates a signature for the payload sent
 * @param {{}} payload 
 */
function generate_signature(payload) {
	const content = JSON.stringify(payload) + api_nets_skey;
	const hash    = sha256().update(content).digest("hex").toUpperCase();
	return (Buffer.from(hash, 'hex').toString('base64'));
}

/**
 * Generate a NETs QR Code
 * @param {Request} request 
 * @param {Response} response 
**/
async function handle_transaction_generate(req, res) {
	try {
		if (!req.body["amount"]) throw Error("Missing required parameter `amount`");
	}
	catch (error) {
		console.error("Malfored Request");
		console.error(error);
		return res.status(400).end();
	}

	try {
		/** @type {import('./nets-payload').OrderRequest} */
		const payload   = create_order(parseInt(req.body["amount"], 10));
		const signature = generate_signature(payload);
		const response  = await Axios.post("https://uat-api.nets.com.sg:9065/uat/merchantservices/qr/dynamic/v1/order/request", payload, {
			headers: {
				"Content-Type": "application/json",
				"KeyId"       : api_nets_key,
				"Sign"        : signature
			}
		});
		
		if (response.status != 200)
			throw Error("Unable to consume external api services");

		/** @type {import('./nets-payload').OrderResponse} */
		const content = response.data;

		if (content.response_code != '00') {
			console.error(`Non successful request`);
			console.error(content);
			throw Error("Unable to consume external api services");
		}
		

		return res.status(200).json({
			"qr_code"         : content.qr_code,
			"stan"            : content.stan,
			"transaction_date": content.transaction_date,
			"transaction_time": content.transaction_time,
			"txn_id"          : content.txn_identifier
		});
	}
	catch (error) {
		console.error("Internal Server Error");
		console.error(error);
		return res.status(500).end();
	}
}


/**
 * Query an existing request
 * @param {Request}  req 
 * @param {Response} res 
**/
async function handle_transaction_query(req, res) {
	try {
		if (!req.body["txn_id"])           throw Error("Missing required parameter `txn_id`");
		if (!req.body["stan"])             throw Error("Missing required parameter `stan`");
		if (!req.body["transaction_date"]) throw Error("Missing required parameter `transaction_date`");
		if (!req.body["transaction_time"]) throw Error("Missing required parameter `transaction_time`");
		if (!req.body["amount"])           throw Error("Missing required parameter `amount`");
	}
	catch (error) {
		console.error("Malfored Request");
		console.error(error);
		return res.status(400).end();
	}

	try {
		/** @type {import('./nets-payload').QueryRequest} */
		const payload = JSON.parse(FileSys.readFileSync(`${__dirname}/nets-qr-query.json`));

		payload.stan             = req.body["stan"];
		payload.txn_identifier   = req.body["txn_id"];
		payload.transaction_date = req.body["transaction_date"];
		payload.transaction_time = req.body["transaction_time"];
		payload.amount           = req.body["amount"];
		payload.npx_data.E201    = req.body["amount"];

		const signature = generate_signature(payload);
		const response  = await Axios.post("https://uat-api.nets.com.sg:9065/uat/merchantservices/qr/dynamic/v1/transaction/query", payload, {
			headers: {
				"Content-Type": "application/json",
				"KeyId"       : api_nets_key,
				"Sign"        : signature
			}
		});
		
		if (response.status != 200)
			throw Error("Unable to consume external api services");

		/** @type {import('./nets-payload').QueryResponse} */
		const content = response.data;

		switch(content.response_code) {
			case "00"   : return res.status(200).json({"code": content.response_code, "status": "Okay"    });
			case "09"   : return res.status(200).json({"code": content.response_code, "status": "Pending" });
			case "68"   : return res.status(200).json({"code": content.response_code, "status": "Expired" });
			     default: return res.status(200).json({"code": content.response_code, "status": "Failed"  });
		}
	}
	catch (error) {
		console.error("Internal Server Error");
		console.error(error);
		return res.status(500).end();
	}
}

/**
 * Void an existing request
 * @param {Request}  req 
 * @param {Response} res 
**/
async function handle_transaction_void(req, res) {
	try {
		if (!req.body["txn_id"])           throw Error("Missing required parameter `txn_id`");
		if (!req.body["stan"])             throw Error("Missing required parameter `stan`");
		if (!req.body["transaction_date"]) throw Error("Missing required parameter `transaction_date`");
		if (!req.body["transaction_time"]) throw Error("Missing required parameter `transaction_time`");
		if (!req.body["amount"])           throw Error("Missing required parameter `amount`");
	}
	catch (error) {
		console.error("Malfored Request");
		console.error(error);
		return res.status(400).end();
	}

	try {
		/** @type {import('./nets-payload').QueryRequest} */
		const payload = JSON.parse(FileSys.readFileSync(`${__dirname}/nets-qr-void.json`));

		payload.stan             = req.body["stan"];
		payload.txn_identifier   = req.body["txn_id"];
		payload.transaction_date = req.body["transaction_date"];
		payload.transaction_time = req.body["transaction_time"];
		payload.amount           = req.body["amount"];
		payload.npx_data.E201    = req.body["amount"];

		const signature = generate_signature(payload);
		const response  = await Axios.post("https://uat-api.nets.com.sg:9065/uat/merchantservices/qr/dynamic/v1/transaction/reversal", payload, {
			headers: {
				"Content-Type": "application/json",
				"KeyId"       : api_nets_key,
				"Sign"        : signature
			}
		});
		
		if (response.status != 200)
			throw Error("Unable to consume external api services");

		/** @type {import('./nets-payload').QueryResponse} */
		const content = response.data;

		switch(content.response_code) {
			case "00"   : return res.status(200).json({"code": content.response_code, "status": "Okay"    });
			case "09"   : return res.status(200).json({"code": content.response_code, "status": "Pending" });
			case "68"   : return res.status(200).json({"code": content.response_code, "status": "Expired" });
			     default: return res.status(200).json({"code": content.response_code, "status": "Failed"  });
		}
	}
	catch (error) {
		console.error("Internal Server Error");
		console.error(error);
		return res.status(500).end();
	}
}