import { SafeString } from 'handlebars'

function sum(...param) { 
	var result = arguments[0];
	for (var i = 1; i < arguments.length - 1; ++i)
		result = result + arguments[i];

	return result;
}

function sub(...param) { 
	var result = arguments[0];
	for (var i = 1; i < arguments.length - 1; ++i)
		result = result - arguments[i];
	return result;
}

function mul(...param) { 
	var result = arguments[0];
	for (var i = 1; i < arguments.length  - 1; ++i)
		result = result * arguments[i];
	return result;
}

function div(...param) { 
	var result = arguments[0];
	for (var i = 1; i < arguments.length  - 1; ++i)
		result = result / arguments[i];
	return result;
}

/**
 * Provides a set of arithmetic helpers
**/
export function arithmetic_helpers() {
	return {
		"op_sub": sub,
		"op_sum": sum,
		"op_mul": mul,
		"op_div": div
	}
}