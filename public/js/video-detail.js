/**
 * Initialization trigger
 * @param {Event} event The window event produced
 */
 window.addEventListener('load', (event) => {
	console.log("Initializing");
	//** First leve code here */
	document.getElementById("imageFile").addEventListener("change", onFileSelected);
	document.getElementById("content-title").addEventListener("input", onTitleChanged);
	console.log("Initialized");
});

/**
 *	Callback function when a file is selected
 *	@param {Event} event 
**/
function onFileSelected(event) {
	const parser        = new FileReader();
	      parser.onload = onFileParsed;
	parser.readAsDataURL(event.target.files[0]);
}

/**
 * Callback function when image file is parsed into base64
 *	@param {ProgressEvent<FileReader>} event 
**/
function onFileParsed(event) {
	document.getElementById("imagePoster").src = event.target.result;
}

/**
 * 
 * @param {InputEvent} event 
 */
function onTitleChanged(event) {

	try {
		if (event.target.value.length < 3) {
			return;
		}
		else {
			const Request = new XMLHttpRequest();
			Request.open("GET", `http://www.omdbapi.com/?apikey=b152570d&s="${event.target.value}"`);
			Request.onreadystatechange = onTitleSearchResponse.bind(this, Request);
			Request.send();
		}
	}
	catch (error) {
		console.error("Failed to make http request");
		console.error(error);
	}
}

/**
 * 
 * @param {XMLHttpRequest} request 
 */
function onTitleSearchResponse(request) {
	if (request.readyState == 4 && request.status == 200) {
		const response = JSON.parse(request.responseText);
		const results  = response["Search"];
		const keywords = results.map((it, idx) => { return { value : it["Title"], id: idx}; });
		$('#content-title').autocomplete({
			source   : keywords,
			minLength: 3,
			select   : function (event, ui) {
				console.log("Selected Content");
				const target                                 = results[ui.item.id];
				const /** @type {HTMLImageElement} **/ image = document.getElementById("imagePoster");
				image.setAttribute("src", target["Poster"]);
				document.getElementById("content-poster-url").value = target["Poster"];
			}
		});
	}
	else {
		console.error(`OneMDB Request failed: ${request.readyState} ${request.status}`);
	}
}
